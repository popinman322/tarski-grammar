
abstract Tarski = {

	flags startcat = Phrase;
	
	cat Phrase; Label;

	fun
	  And : Phrase -> Phrase -> Phrase;
	  Or  : Phrase -> Phrase -> Phrase;
	  Imp : Phrase -> Phrase -> Phrase;
	  -- Not : Phrase -> Phrase;

	  BlockLabel : Label;
	  Predicate : Label -> Phrase;
	  Relation : Label -> Label -> Phrase;
}

