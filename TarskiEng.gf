--# -path=.:present

concrete TarskiEng of Tarski =
  open SyntaxEng,
       ParadigmsEng,
       SentenceEng,
       Prelude in {
	
	lincat Phrase = S;
	       Label = NP;

	lin And left right = mkS and_Conj left right;
	    Or  left right = mkS or_Conj left right;
	    Imp left right = variants {
	    	mkS if_then_Conj left right |
	    	mkS (mkConj "implies") left right
	    };
	    -- Not phrase = mkS presentTense simultaneousAnt negativePol phrase;

	    BlockLabel = variants {mkLabel "A" | mkLabel "B" | mkLabel "C"};
	    Predicate label = mkS presentTense simultaneousAnt positivePol (variants {
	    	mkCl label (mkN "cube") |
	    	mkCl label (mkN "sphere") |
	    	mkCl label (mkN "tetrahedron")
	    });

	oper mkLabel : Str -> NP = \s -> mkNP (mkN s);
}
